/** @type {import('next').NextConfig} */
const path = require("path")

const nextConfig = {
	reactStrictMode: true,
	images: {
		domains: ["randomuser.me"],
	},
	sassOptions: {
		includePaths: [path.join(__dirname, "styles")],
	},
	redirects: async () => {
		return [
			{
				source: "/login",
				destination: "/",
				permanent: true,
			},
			{
				source: "/contact",
				destination: "/",
				permanent: true,
			},
		]
	},
	rewrites: async () => {
		return [
			{
				source: "/ab",
				destination: "/about",
				// permanent: true,
			},
			{
				source: "/abt",
				destination: "/about",
				// permanent: true,
			},
		]
	},
}

module.exports = nextConfig
