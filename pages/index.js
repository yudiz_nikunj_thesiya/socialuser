import axios from "axios"
import Head from "next/head"
import Image from "next/image"
import UserCard from "../shared/components/UserCard"
import styles from "../assets/scss/home.module.scss"
import { useRouter } from "next/router"
import { useState } from "react"
import { BeatLoader } from "react-spinners"
import { IoRefreshOutline } from "react-icons/io5"

export default function Home({ allUsers, getResults }) {
	const router = useRouter()
	const [usersData, setUsersData] = useState(allUsers)
	const [loading, setLoading] = useState(false)
	const [results, setResults] = useState(20)

	const loadMore = () => {
		setResults(results + 10)
		setLoading(true)

		axios
			.get(`https://randomuser.me/api/?seed=foobar&results=${results}`)
			.then((response) => {
				setUsersData(response?.data?.results)
				setLoading(false)
			})
	}

	return (
		<>
			<Head>
				<title>Home</title>
				<meta name="description" content="list of usersocial users" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<div className={styles.home}>
				<div className={styles.user_container}>
					{usersData.map((user, index) => (
						<UserCard user={user} key={index} />
					))}
				</div>
				{loading === true ? (
					<button className={styles.loadmore}>
						<BeatLoader color="#7171f2" loading={loading} size={5} />
					</button>
				) : (
					<button className={styles.loadmore} onClick={loadMore}>
						<span>Load More</span>
						<IoRefreshOutline />
					</button>
				)}
			</div>
		</>
	)
}

export async function getServerSideProps({ query: { results = 10 } }) {
	let data = await axios.get(
		`https://randomuser.me/api/?seed=foobar&results=${results}`
	)
	let allUsers = await data?.data?.results

	return {
		props: { allUsers, getResults: parseInt(results) },
	}
}
