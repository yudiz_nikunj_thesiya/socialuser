import axios from "axios"
import React from "react"
import { useRouter } from "next/router"
import Image from "next/image"
import styles from "../../assets/scss/user.module.scss"
import moment from "moment"
import {
	HiOutlineArrowLeft,
	HiOutlineLocationMarker,
	HiOutlineMail,
} from "react-icons/hi"
import { FiPhone } from "react-icons/fi"
import { FaBirthdayCake } from "react-icons/fa"
import { CgUser } from "react-icons/cg"
import { BsGenderMale, BsGenderFemale } from "react-icons/bs"
import Head from "next/head"
import Link from "next/link"

const User = ({ user }) => {
	const router = useRouter()
	const { userId } = router.query

	console.log(user)

	return (
		<>
			<Head>
				<title>User Details</title>
				<meta name="description" content="About UserSocial" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<div className={styles.user_container}>
				<div className={styles.backBtn}>
					<Link href="/">
						<div className={styles.backBtn_btn}>
							<HiOutlineArrowLeft />
						</div>
					</Link>
				</div>
				<div className={styles.user}>
					<div
						className={styles.userImg}
						style={{
							width: "150px",
							height: "150px",
							position: "relative",
						}}
					>
						<Image
							src={user.picture.large}
							width={100}
							height={100}
							alt="user"
							layout="fill"
							objectFit="contain"
						/>
					</div>
					<h3
						className={styles.user_name}
					>{`${user.name.title} ${user.name.first} ${user.name.last}`}</h3>
					<p className={styles.user_id}>@{user?.login?.username}</p>
					<div className={styles.user_details}>
						<div className={styles.email}>
							<span className={styles.email_icon}>
								<HiOutlineMail />
							</span>
							<p>{user?.email}</p>
						</div>
						<div className={styles.phone}>
							<span className={styles.phone_icon}>
								<FiPhone />
							</span>
							<p>{user?.phone}</p>
						</div>
						<div className={styles.dob}>
							<span className={styles.dob_icon}>
								<FaBirthdayCake />
							</span>
							<p>{moment(user?.dob?.date).format("MMMM Do YYYY")}</p>
						</div>
						<div className={styles.age}>
							<span className={styles.age_icon}>
								<CgUser />
							</span>
							<p>{user?.dob?.age}</p>
						</div>
						<div className={styles.gender}>
							<span className={styles.gender_icon}>
								{user?.gender === "male" ? (
									<BsGenderMale />
								) : (
									<BsGenderFemale />
								)}
							</span>
							<p>{user?.gender}</p>
						</div>
					</div>
					<div className={styles.address}>
						<span className={styles.address_icon}>
							<HiOutlineLocationMarker />
						</span>
						<div>
							<p>{`${user?.location?.street?.number} ${user?.location?.street?.name},`}</p>
							<p>{`${user?.location?.city} ${user?.location?.state}, ${user?.location?.postcode},`}</p>
							<p>{`${user?.location?.country}.`}</p>
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default User

export async function getServerSideProps(context) {
	console.log("context", context)
	let data = await axios.get(
		`https://randomuser.me/api/?username=${context.userId}`
	)
	console.log(data.data.results)
	let user = await data?.data.results[0]

	return {
		props: { user },
	}
}
