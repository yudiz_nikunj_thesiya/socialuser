import Head from "next/head"
import React from "react"
import styles from "../assets/scss/about.module.scss"

const about = () => {
	return (
		<>
			<Head>
				<title>About</title>
				<meta name="description" content="About UserSocial" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<div className={styles.about}>
				<h1 className={styles.about_title}>About</h1>
				<p className={styles.about_desc}>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas
					dignissimos odit sapiente ipsa exercitationem dolorum dolor porro odio
					placeat ipsam nisi enim eius ipsum culpa eum, corporis modi,
					laboriosam sint. Lorem ipsum dolor sit amet, consectetur adipisicing
					elit. Animi, similique sapiente nihil atque rerum enim provident
					blanditiis porro id praesentium quisquam, vitae eaque fuga, molestiae
					distinctio repellat sed voluptates sequi. Lorem ipsum dolor sit amet
					consectetur adipisicing elit. Illo minus nesciunt delectus, in
					inventore soluta obcaecati dolore laudantium provident harum quod
					corporis accusantium deserunt corrupti ullam ratione molestias commodi
					cupiditate? Lorem ipsum dolor sit amet, consectetur adipisicing elit.
					Facere dignissimos deleniti ut eveniet accusantium itaque illum autem
					in. Nisi ullam tempora asperiores illum labore vero doloremque beatae
					est eos veniam? Lorem ipsum, dolor sit amet consectetur adipisicing
					elit. Veritatis animi non harum laborum, quibusdam eum libero quis,
					deleniti delectus accusantium est incidunt architecto, labore
					aspernatur laboriosam nesciunt nam magni hic.
				</p>
			</div>
		</>
	)
}

export default about
