import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { BeatLoader } from "react-spinners"
import "../assets/scss/globals.scss"
import Layout from "../shared/components/Layout"

function MyApp({ Component, pageProps }) {
	const router = useRouter()
	const [loading, setLoading] = useState(false)

	useEffect(() => {
		const handleStart = (url) => {
			setLoading(true)
		}
		const handleStop = () => {
			setLoading(false)
		}

		router.events.on("routeChangeStart", handleStart)
		router.events.on("routeChangeComplete", handleStop)
		router.events.on("routeChangeError", handleStop)

		return () => {
			router.events.off("routeChangeStart", handleStart)
			router.events.off("routeChangeComplete", handleStop)
			router.events.off("routeChangeError", handleStop)
		}
	}, [router])

	return loading ? (
		<div className="transition">
			<BeatLoader color="#fff" loading={loading} size={50} />
		</div>
	) : (
		<Layout>
			<Component {...pageProps} />
		</Layout>
	)
}

export default MyApp
