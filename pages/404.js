import Head from "next/head"
import React from "react"
import styles from "../assets/scss/404.module.scss"
const ErrorPage = () => {
	return (
		<>
			<Head>
				<title>Page Not Found!</title>
				<meta name="description" content="page not found" />
				<link rel="icon" href="/favicon.ico" />
			</Head>
			<div className={styles.err}>
				<h1 className={styles.err_title}>404</h1>
				<p className={styles.err_desc}>OOPS! Page not found</p>
			</div>
		</>
	)
}

export default ErrorPage
