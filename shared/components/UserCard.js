import Image from "next/image"
import React from "react"
import styles from "../../assets/scss/usercard.module.scss"
import { FiPhone } from "react-icons/fi"
import { HiOutlineMail } from "react-icons/hi"
import Link from "next/link"

const UserCard = ({ user }) => {
	return (
		<Link href={`/user/${user?.login?.username}`}>
			<div className={styles.card}>
				<div
					className={styles.card_img}
					style={{
						width: "100px",
						height: "100px",
						position: "relative",
					}}
				>
					<Image
						src={user.picture.large}
						width={100}
						height={100}
						alt="user"
						layout="fill"
						objectFit="contain"
					/>
				</div>
				<div className={styles.card_details}>
					<div
						className={styles.card_details__name}
					>{`${user.name.title} ${user.name.first} ${user.name.last}`}</div>
					<div className={styles.card_details__email}>
						<span className={styles.card_details__email_icon}>
							<HiOutlineMail />
						</span>
						<span>{user?.email}</span>
					</div>
					<div className={styles.card_details__phone}>
						<span className={styles.card_details__phone_icon}>
							<FiPhone />
						</span>
						<span>{user?.phone}</span>
					</div>
				</div>
			</div>
		</Link>
	)
}

export default UserCard
