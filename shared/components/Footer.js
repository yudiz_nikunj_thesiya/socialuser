import React from "react"
import styles from "../../assets/scss/footer.module.scss"

const Footer = () => {
	return (
		<div className={styles.footer}>
			<p>All rights reserved. Designed by usersocial</p>
		</div>
	)
}

export default Footer
