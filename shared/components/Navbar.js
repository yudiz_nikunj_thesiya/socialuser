import Link from "next/link"
import { useRouter } from "next/router"
import React from "react"
import styles from "../../assets/scss/navbar.module.scss"

const Navbar = () => {
	const router = useRouter()
	const path = router.pathname

	return (
		<div className={styles.navbar}>
			<Link href="/">
				<span className={styles.logo}>USERSOCIAL</span>
			</Link>

			<div className={styles.navbar_nav}>
				<Link href="/" passHref>
					<p className={path === "/" ? styles.active : styles.inactive}>Home</p>
				</Link>
				<Link href="/about" passHref>
					<p className={path === "/about" ? styles.active : styles.inactive}>
						About
					</p>
				</Link>
				<Link href="/contact" passHref>
					<p className={path === "/contact" ? styles.active : styles.inactive}>
						Contact
					</p>
				</Link>
			</div>
		</div>
	)
}

export default Navbar
